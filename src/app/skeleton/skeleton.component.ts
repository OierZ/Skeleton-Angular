import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'skeleton',
  templateUrl: './skeleton.component.html',
  styleUrls: ['./skeleton.component.scss'],
})
export class SkeletonComponent implements OnInit {
  @Input() type: string = 'line';
  @Input() size: string = 'md';

  validTypes = ['card', 'image', 'line'];
  validSizes = ['xs', 'sm', 'md', 'lg', 'xl'];

  isValidType(): boolean {
    return this.validTypes.includes(this.type);
  }

  isValidSize(): boolean {
    return this.validSizes.includes(this.size);
  }

  constructor() {}

  ngOnInit(): void {}
}
